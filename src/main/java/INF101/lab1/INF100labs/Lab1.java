package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/1/
 */
public class Lab1 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        task1();

        task2();

    }

    public static void task1() {
        System.out.println("Hei, det er meg, datamaskinen.");
        System.out.println("Hyggelig å se deg her.");
        System.out.println("Lykke til med INF101!");

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line

        System.out.print("Hva er ditt navn? ");
        String navn = sc.nextLine();

        System.out.print("Hva er din adresse? ");
        String adresse = sc.nextLine();

        System.out.print("Hva er ditt postnummer og poststed? ");
        String postnummer_poststed = sc.nextLine();
        sc.close();

        System.out.println("\n" + navn + " sin adresse er:");
        System.out.println();
        System.out.println(navn);
        System.out.println(adresse);
        System.out.println(postnummer_poststed);
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }


}
